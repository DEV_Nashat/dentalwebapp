<?php

namespace App\Http\Livewire;

use App\Models\Dentist as DentistModel;
use Livewire\Component;

class Dentist extends Component
{
    public function mount()
    {
    }
    public function render()
    {
        return view('livewire.dentist', [
            'dentists' => DentistModel::all()
        ]);
    }
}