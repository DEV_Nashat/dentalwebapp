<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dentist extends Model
{
    use HasFactory;

    protected $table = 'Management.Dentist';
    protected $primaryKey = 'DentistID';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}