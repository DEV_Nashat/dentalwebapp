<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Dentist', function (Blueprint $table) {
            $table->id('DentistID');
            $table->string('DentistName');
            $table->string('Specilist')->nullable();
            $table->text('Address')->nullable();
            $table->string('Phone1')->nullable();
            $table->string('Phone2')->nullable();
            $table->unsignedDecimal('SalaryPercentage')->nullable();
            $table->uuid('AccountID')->nullable();
            $table->boolean('IsSalary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Dentist');
    }
};